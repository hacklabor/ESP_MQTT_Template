// WiFi settings
const char* ssid = "SSID";
const char* password = "PSK";
char* espHostname = "Hostname";

// OTA settings
#define ota_password "OTAPASSWORD"

// MQTT server settings
const char* mqtt_server = "MQTT broker hostname, FQDN or IP address";
int mqtt_server_port = 1883;

// MQTT subscribe settings
const char* mainTopic = "Topic/#";

// MQTT Last Will and Testament settings
byte willQoS = 0;
const char* willTopic = "lwt/esp-topic";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-ClientID";
boolean willRetain = true;
