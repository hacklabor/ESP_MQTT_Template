## MQTT example for ESP8266 with LWT and OTA
ready to use MQTT code example for an ESP8266 with built-in LWT and OTA based on the [pubsubclient library](https://github.com/knolleary/pubsubclient "pubsubclient library") and the [ArduinoOTA library ](https://github.com/esp8266/Arduino/tree/master/libraries/ArduinoOTA "ArduinoOTA library") 

LWT (Last Will and Testament) is used to notify other clients of a disconnect
OTA (Over The Air) is used to update the ESP8266 wirelessly

##### how to use
1. install (if not done yet) the [ESP8266 core](https://github.com/esp8266/Arduino) to flash the ESP8266 later
2. Install the library! Launch the IDE, click Sketch and then Include Library > Manage Libraries. Then search for PubSubClient and install the latest version
3. copy or rename config-example.h to config.h <br>
3.1. edit WiFi, OTA and MQTT settings in the config.h
4. implement your own code

##### OTA
- After flashing for the first time via serial, the ESP8266 can be found under network interfaces at Ports
- Before flashing first time via OTA, reset device manually, otherwise OTA password will
  not be accepted
