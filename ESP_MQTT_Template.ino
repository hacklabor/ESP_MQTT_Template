#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoOTA.h>

extern "C" {
#include "user_interface.h" // to set ESP hostname
}

#include "config.h"

// WiFi
WiFiClient espClient;
PubSubClient client(espClient);


void setup() {
  Serial.begin(115200);

  setup_wifi();
  setup_ota();

  client.setServer(mqtt_server, mqtt_server_port);
  client.setCallback(callback);
}


void setup_wifi() {
  WiFi.mode(WIFI_STA); // set WiFi mode, no AP
  WiFi.begin(ssid, password);

  String WiFiConnectionStringOne = "Connecting to WiFi network ";
  String WiFiConnectionStringTwo = "...";
  String WiFiConnectionStringComplete = WiFiConnectionStringOne + "'" + ssid + "'" + WiFiConnectionStringTwo;
  Serial.println();
  Serial.print(WiFiConnectionStringComplete);

  // check if connected to WiFi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println(" success!");

  wifi_station_set_hostname(espHostname); // set ESP hostname

  Serial.println();
  Serial.print("Hostname:    ");
  Serial.println(WiFi.hostname());
  Serial.print("IP address:  ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
  Serial.println();
}  

void setup_ota() {
  ArduinoOTA.setHostname(espHostname);

  ArduinoOTA.setPassword(ota_password);

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
}


void setup_mqtt() {
  String MQTTConnectionStringOne = "Connecting to MQTT server ";
  String MQTTConnectionStringTwo = "...";
  String MQTTConnectionStringComplete = MQTTConnectionStringOne + "'" + mqtt_server + "'" + MQTTConnectionStringTwo;
  Serial.println();
  Serial.print(MQTTConnectionStringComplete);

  // check if connected to MQTT server
  while (!client.connected()) {
    if (!client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      delay(1000);
      Serial.print(".");
    } else {
      Serial.println(" success!");
      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);
      // ... and resubscribe
      client.subscribe(mainTopic);
    }
  }
}


void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}


void loop() {
  if (WiFi.status() != WL_CONNECTED) {
    setup_wifi();
  }
  if (!client.connected()) {
    setup_mqtt();
  }
  client.loop();
  ArduinoOTA.handle();
}
